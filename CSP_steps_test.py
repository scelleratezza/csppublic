from CSP_steps import *


def check(a, b):
    debugprint(1, "Check called with parameters", a, b)
    return a != b


def check_enforceconsistency():
    state = CSPState()
    state.domain = [range(5), [1], [2], [3, 4], [4]]
    pairs = [(i, j) for i in range(5) for j in range(5) if i != j]

    expectedsolution = CSPState()
    expectedsolution.domain = [[x] for x in range(5)]

    # build constraint list
    constraintslist = []
    for f, s in pairs:
        c = Constraint()
        c.nodesinvolved = [f, s]
        c.checkfunction = check
        constraintslist.append(c)

    # create constraint graph
    # cset = Constraintset(constraintslist)

    # create constraint satisfaction class
    csp = CSPSteps(constraintslist)

    csp.enforce_arc_consistency(state)

    print state
    print expectedsolution
    assert (state.domain == expectedsolution.domain)


basestate = CSPState()
basestate.domain.append([5, 1, 2, 3])
basestate.domain.append([5, 1, 2])
basestate.domain.append([1, 2, 3])
pairs = [(0, 1), (1, 2), (2, 0)]

constraintslist = []
for f, s in pairs:
    c = Constraint()
    c.nodesinvolved = [f, s]
    c.checkfunction = check
    constraintslist.append(c)

cset = Constraintset(constraintslist)

csp = CSPSteps(constraintslist)
newstate = basestate.copy()

solution = csp.solve(newstate)
print "Base:", newstate
print "Solution:", solution

check_enforceconsistency()