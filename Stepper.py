import sys


class Stepper():
    def __init__(self, f=None, fparams=None):
        if fparams is None:
            fparams = []
        if type(fparams) not in [list, tuple]:
            fparams = [fparams]
        if not callable(f):
            sys.stderr.write("WARNING: Stepper constructor called with non callable first function\n")

        self._next = (f, fparams)
        self._tailstack = []
        self._retval = None

    def next(self):
        if self._next is None:
            if len(self._tailstack) > 0:
                self._next = self._tailstack.pop()
            else:
                # END of execution
                return None  # something ..
                # or raise something

        nextf, nextparams = self._next
        self._retval, self._next, tail = nextf(*nextparams)

        if Stepper.halt == self._retval:
            del self._tailstack[:]
            return None

        if tail is not None:
            self._tailstack.append(tail)

        return True  # col ch'et per

    @staticmethod
    def halt():
        pass
