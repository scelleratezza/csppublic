from CSP import *
from Stepper import Stepper

from heapq import *

#
# Interface
# csp = CSPSteps()
# csp. ... # blahblah (initialize)
# csp.solve_init(state)
# while csp.solve_next():
# csp.solve_print()
# ... # do stuff with current state
#


def debugprint(level, *args):
    if level <= alertlevel:
        sys.stderr.write(' '.join([str(x) for x in args]) + '\n')


alertlevel = 0


class CSPSteps:
    def __init__(self, constraintslist):
        self.constraintslist = constraintslist
        self.constraintgraph = Constraintset(constraintslist)
        self.stepper = None

        self.solution = None

    # # BROKEN ? requires the correct ordering for
    def remove_values(self, state, constraintarc):
        def check_valid_combination(currentchoice=None, idx=0):
            if currentchoice is None:
                currentchoice = [None] * nvar

            if nvar == idx:
                return constraintarc.constraint.checkfunction(*currentchoice)
            else:
                curnode = nodesinvolved[idx]
                if curnode == constraintarc.srcnode:
                    currentchoice[idx] = srcvalue
                    return check_valid_combination(currentchoice, idx + 1)
                else:
                    valid = 0
                    for v in state.domain[curnode]:
                        currentchoice[idx] = v
                        if check_valid_combination(currentchoice, idx + 1):
                            valid = 1
                            break
                    return valid

        debugprint(1, constraintarc)
        nodesinvolved = constraintarc.constraint.nodesinvolved
        nvar = len(nodesinvolved)
        toremove = []
        dsrc = state.domain[constraintarc.srcnode]
        for i, srcvalue in enumerate(dsrc):

            validcombinationexists = check_valid_combination()
            if not validcombinationexists:
                toremove.append(i)

        for i in toremove[-1::-1]: del dsrc[i]

        return len(toremove)

    # def enforce_arc_consistency(self, state):
    def enforce_arc_consistency(self, state, node=None):
        #
        # enforces arcs after a change in the domain of node
        # if node is None, all nodes are updated
        #
        totalremovedvalues = 0
        if node is None:
            for n in range(len(state.domain)):
                totalremovedvalues += self.enforce_arc_consistency(state, n)
            return totalremovedvalues

        debugprint(1, ">>Enforcing arc consistency on state ", state)
        constraintset = self.constraintgraph

        # queue = []
        queue = set()
        # initialize: all constraints must be examined at least once
        # for constraintarc in constraintset.arclist:
        # for _, constraintarc in constraintset.arclist.iteritems():
        for constraintarc in constraintset.arcsendingin[node]:
            debugprint(1, constraintarc)
            # queue.append(constraintarc)
            queue.add(constraintarc)

        while True:
            try:
                constraintarc = queue.pop()
            except (IndexError, KeyError):
                break  # queue empty
            debugprint(0, "currently %i arcs in the queue" % len(queue))
            debugprint(0, "meh:", constraintarc)
            nremovedvalues = self.remove_values(state, constraintarc)
            debugprint(0, "REMOVED", nremovedvalues)

            if nremovedvalues > 0:
                # # neighbourset should contain all neighbours, or in alternative
                # # all constraints with an end on the current element
                # for neighbour in constraintset.neighbourlist[constraintarc.srcnode]:
                totalremovedvalues += nremovedvalues
                for neighbour in constraintset.arcsendingin[constraintarc.srcnode]:
                    # queue.append(neighbour)
                    debugprint(1, "Adding neighbour ", neighbour)
                    queue.add(neighbour)

        debugprint(1, "<<Enforcing arc consistency DONE")
        return totalremovedvalues

    def solve_init(self, basestate):
        debugprint(0, "INSIDE solve")
        # make sure the initial state is compliant
        solution = basestate.copy()
        # self.enforce_arc_consistency(solution)
        if solution.allvaluesset():
            # we have a solution
            return True, solution
        elif not solution.isvalid():
            # domain's already dried up
            return False, None

        # # depth first search
        self.DEBUG_n_solve_iterative = 0
        self.DEBUG_depth_solve_iterative = 0

        self.stepper = Stepper(self.solve_iterative_get_nchanges_head, solution)
        return True, None

    def next(self):
        return self.stepper.next()

    def solve(self, basestate):
        debugprint(0, "INSIDE solve")

        is_valid, solution = self.solve_init(basestate)
        if is_valid:
            if solution is not None:
                return solution
            else:
                while self.stepper.next():
                    pass
            solution = self.solution
            debugprint(0, "TOTAL:", self.DEBUG_n_solve_iterative)
            return solution
        else:
            return None

    def solve_iterative_get_nchanges_head(self, state):
        debugprint(0, "INSIDE solve_iterative_get_nchanges_HEAD")
        # if state is has one element per element we have a solution
        if state.allvaluesset():
            # (all the checks have been passed, i.e.)
            # return True
            self.solution = state
            return Stepper.halt, None, None
        # else
        else:
            # get node with the least number of available options
            node = state.get_mindomain()
            # the node ought not to be None, as this case should have been caught previously,
            # and we take for granted that the initial state is both valid and not empty
            assert (node is not None)

            values = state.domain[node]

            toexamine = []

            head = (self.solve_iterative_get_nchanges_body, (state, node, values, toexamine))
            tail = (self.solve_iterative_loop_on_values, (toexamine,))

            return values, head, tail

    def solve_iterative_get_nchanges_body(self, state, node, values, toexamine):
        debugprint(0, "INSIDE solve_iterative_get_nchanges_BODY")
        if 0 == len(values):
            return None, None, None
        value = values.pop()
        # modify tomodifystate, and pass this to successive functions
        tomodifystate = state.copy()

        tomodifystate.domain[node] = [value]
        nchanges = self.enforce_arc_consistency(tomodifystate, node)
        # check if we got a solution
        if tomodifystate.allvaluesset():
            self.solution = tomodifystate
            return Stepper.halt, None, None
        # if still valid domain
        if tomodifystate.isvalid():
            # toexamine.append( nchanges, value, tomodifystate )
            # toexamine.append((nchanges, tomodifystate))
            heappush(toexamine, (nchanges, tomodifystate))

        head = (self.solve_iterative_get_nchanges_body, (state, node, values, toexamine))
        tail = None

        return (nchanges, tomodifystate), head, tail

    def solve_iterative_loop_on_values(self, toexamine):
        if 0 == len(toexamine):
            return None, None, None

        _, state = heappop(toexamine)

        head = (self.solve_iterative_get_nchanges_head, (state,))
        tail = (self.solve_iterative_loop_on_values, (toexamine,))

        return state, head, tail