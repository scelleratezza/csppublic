# Sudoku
import numpy as np
from math import sqrt
from CSPBacktracerSteps import *
from CSPSimulatedAnnealing import *
from CSPSimulatedAnnealingOptimize import CSPSimulatedAnnealingOptimize


class Sudoku():
    def __init__(self, n=9):
        self.n = n
        self.ncallsconstraint = 0
        self.board = np.zeros((n, n))

        self.basicdomain = CSPState()
        self.basicdomain.domain = [np.arange(n) + 1 for _ in np.nditer(self.board)]

        self.constraintslist = self.build_constraints()
        self.cset = Constraintset(self.constraintslist)

        self.csp = CSPBacktracerSteps(self.constraintslist)
        self.cspi = CSPBacktracerSteps(self.constraintslist, True)
        self.cspmin = CSPMinConflicts(self.constraintslist)
        # self.cspsa = CSPSimulatedAnnealing(self.constraintslist, self.sa_temperatureschedule)
        self.cspsa = CSPSimulatedAnnealing(self.constraintslist, self.sa_temperatureschedule, self.get_nextstate)
        self.cspsao = CSPSimulatedAnnealingOptimize(self.constraintslist, self.sa_temperatureschedule,
                                                    self.get_nextstate, self.optimize_state)

        self.basestate = None
        self.lastunsatisfiedconstraints = None
        self.nstatestochange = n
        self.nrecordedstates = 100
        self.nlaststate = 0
        self.laststates = [None] * self.nrecordedstates

    @staticmethod
    def is_perfect_square(l):
        if int != type(l):
            return False, None
        guess = int(sqrt(l))
        if guess * guess == l:
            return True, guess
        return False, None

    def solve_sao(self, basestate, state):
        self.basestate = basestate
        self.cspsao.solve(basestate, state)

        # # values is of the form [(i,j,v)]

    def makestate_fromboundvalues(self, values):
        def pair_to_ind(idx, jdx):
            return idx * self.n + jdx

        state = self.basicdomain.copy()
        for i, j, v in values:
            state.domain[pair_to_ind(i, j)] = [v]

        return state

    def sa_temperatureschedule(self, time):
        # each constraint is one at the moment, so that at most three constraints per node can be not satisfied
        # delta is thus 3 at max
        # in N steps, we want to pass from delta/T ~= 1 to delta/T ~= 10^-6
        # N = 10**10
        n = 10 ** 10
        return 27.0 / (1 + (10.0 ** 6 * time) / n)

    # return 0.00001

    def print_state_raw(self, state):
        rows = [[x + i * self.n for x in range(self.n)] for i in range(self.n)]
        print ''.join(
            [("%s\t" * self.n) % tuple([state.domain[x][0] if len(state.domain[x]) == 1 else '*' for x in row]) + '\n'
             for row in rows])

    def print_state(self, state):
        is_square, root = self.is_perfect_square(self.n)
        if not is_square:
            return self.print_state_raw(state)
        else:
            rows = [[x + i * self.n for x in range(self.n)] for i in range(self.n)]
            toprint = []
            for row in rows:
                rowstr = []
                for idx in range(root):
                    idxstart = root * idx
                    idxend = root * (idx + 1)
                    rowstr.append("%s " * root % tuple(
                        state.domain[x][0] if len(state.domain[x]) == 1 else '*' for x in row[idxstart:idxend]))

                toprint.append("| ".join(rowstr) + '\n')
            toprintgrp = []
            for idx in range(root):
                idxstart = root * idx
                idxend = root * (idx + 1)
                toprintgrp.append(''.join(toprint[idxstart:idxend]))
            hline = '-' * (len(toprint[0]) - 2) + '\n'
            print hline.join(toprintgrp)

    def get_nextstate(self, oldstate, basestate):
        newstate = oldstate.copy()
        nbrokenconstraints = np.zeros(len(newstate.domain))

        # # get broken constraints per node
        for node in range(len(newstate.domain)):
            nbrokenconstraints[node] = self.cspsa.count_brokenconstraints(newstate, node)

        # # select N nodes to change
        ntochange = self.nstatestochange
        nodestochange = [i for i, (_, dom) in
                         sorted(enumerate(zip(nbrokenconstraints, basestate.domain)), key=lambda x: -x[1][0])[
                         :ntochange] if len(dom) > 1]

        # # change each node to the best different option
        delta = 0
        for node in nodestochange:
            curdom = basestate.domain[node]
            newstate.domain[node] = [curdom[randint(0, len(curdom) - 1)]]
            delta += self.cspsa.count_brokenconstraints(newstate, node) - nbrokenconstraints[node]

        # # return results
        return newstate, delta

    def build_constraints(self):
        def pairs2ind(cp):
            n = self.n
            return [[idx * n + jdx for idx, jdx in x] for x in cp]

        constraintslist = []

        hconstraintindices = pairs2ind([[(i, j) for j in range(self.n)] for i in range(self.n)])
        vconstraintindices = pairs2ind([[(j, i) for j in range(self.n)] for i in range(self.n)])
        is_square, root = self.is_perfect_square(self.n)
        if is_square:
            sqlist = [(i, j) for j in range(root) for i in range(root)]
            sconstraintindices = pairs2ind(
                [[(root * i1 + i2, root * j1 + j2) for i2, j2 in sqlist] for i1, j1 in sqlist])
        else:
            sconstraintindices = []

        print [len(x) for x in [hconstraintindices, vconstraintindices, sconstraintindices]]
        for curlist in [hconstraintindices, vconstraintindices, sconstraintindices]:
            for nodes in curlist:
                c = Constraint()
                c.nodesinvolved = nodes
                c.checkfunction = self.constraint
                constraintslist.append(c)

        return constraintslist

    def constraint(self, *args):
        # # all different and covering 1 to N
        # # args must be exactly N elements
        # # it's taken for granted that all values in args are between 1 and self.N
        assert (self.n == len(args))
        self.ncallsconstraint += 1
        chk = np.zeros(self.n + 1, dtype=np.int)
        for arg in args:
            if chk[arg] > 0 or arg < 1 or arg > self.n:
                return False
            chk[arg] += 1
        return True

    def is_in_laststates(self, state):
        for st in self.laststates:
            if state == st:
                return True
        return False

    def optimize_state(self, state):
        # # run cspmin until some convergence criterion
        is_valid, solution = self.cspmin.solve_init(self.basestate, state)
        if solution:
            print "Is solution"
            return solution
        else:
            lastres = None
            tolerance = 10000
            neq = 0
            while lastres != tolerance and neq < tolerance:
                if not self.cspmin.next(): break
                curunsatisfiedconstraints = self.cspsa.count_totalbrokenconstraints(self.cspmin.currentstate)
                if curunsatisfiedconstraints < self.lastunsatisfiedconstraints:
                    neq = 0
                else:
                    neq += 1

                self.lastunsatisfiedconstraints = curunsatisfiedconstraints

            if neq == tolerance or self.is_in_laststates(self.cspmin.currentstate):
                self.nstatestochange = int(self.nstatestochange * 1.5)
                print "Doubling n states to change:", self.nstatestochange
            else:
                self.laststates[self.nlaststate % self.nrecordedstates] = self.cspmin.currentstate.copy()
                self.nlaststate += 1

            return self.cspmin.currentstate

# # TODO: need to find some way to expand the domain of a few nodes and run csp backtracing
