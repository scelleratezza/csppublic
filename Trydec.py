class Trydec():
    def debugdec(self, func, *args):
        self.counter += 1
        self.depth += 1
        print "IN", self.counter, self.depth
        func(*args)
        self.depth -= 1
        print "OUT", self.counter, self.depth

    def gigi_d(self, n=5):
        if 0 == n:
            return 1
        return n * self.gigi_d(n - 1)

    def __init__(self):
        self.counter = 0
        self.depth = 0

        # self.gigi_D = self.gigi
        self.gigi = self.debugdec(self.gigi_d)