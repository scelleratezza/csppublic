import sys

from collections import defaultdict


def debugprint(level, *args):
    if level <= alertlevel:
        sys.stderr.write(' '.join([str(x) for x in args]) + '\n')


alertlevel = 0


class CSPState():
    def __init__(self):
        self.domain = []

    def isvalid(self):
        return all([len(x) > 0 for x in self.domain])

    def allvaluesset(self):
        return all([len(x) == 1 for x in self.domain])

    def __eq__(self, other):
        if isinstance(other, CSPState):
            return self.domain == other.domain
        else:
            return NotImplemented

    def get_mindomain(self):
        lengths = [(len(x), i) for i, x in enumerate(self.domain)]
        try:
            firstvalid = (idx for L, idx in sorted(lengths) if L > 1).next()
        except StopIteration:
            return None
        return firstvalid

    def copy(self):
        ret = CSPState()
        for var in self.domain:
            ret.domain.append([v for v in var])
        return ret

    def set_to_state(self, src):
        self.domain = [v for v in src.domain]

    def __str__(self):
        retstr = "{ "
        for i, d in enumerate(self.domain):
            retstr += "%i: %s, " % (i, str(d))
        retstr = retstr[:-2] + " }"
        return retstr


class Constraint():
    def __init__(self):
        self.nodesinvolved = []
        # # checkfunction should be a function of all the variables in nodesinvolved
        self.checkfunction = None


class ConstraintArc():
    def __init__(self):
        self.srcnode = None
        # self.dstnodes = []
        self.constraint = None


class Constraintset():
    def __init__(self, constraintslist):
        self.arclist = {}
        self.neighbourlist = defaultdict(set)
        self.arcsendingin = defaultdict(set)

        self.populate_arclist(constraintslist)

    def populate_arclist(self, constraintslist):
        for constraint in constraintslist:
            for node in constraint.nodesinvolved:
                debugprint(1, node)
                arc = ConstraintArc()
                arc.srcnode = node
                # arc.dstnode = []
                arc.constraint = constraint
                self.arclist[(arc.srcnode, constraint)] = arc
                for dstnode in arc.constraint.nodesinvolved:
                    self.neighbourlist[node].add(dstnode)
                    self.arcsendingin[dstnode].add(arc)
            self.neighbourlist[node].remove(node)


class CSP:
    def __init__(self, constraintslist):
        self.constraintslist = constraintslist
        self.constraintgraph = Constraintset(constraintslist)

        # # BROKEN ? requires the correct ordering for

    def remove_values(self, state, constraintarc):
        def check_valid_combination(currentchoice=None, idx=0):
            if currentchoice is None:
                currentchoice = [None] * nvar

            if nvar == idx:
                return constraintarc.constraint.checkfunction(*currentchoice)
            else:
                curnode = nodesinvolved[idx]
                if curnode == constraintarc.srcnode:
                    currentchoice[idx] = srcvalue
                    return check_valid_combination(currentchoice, idx + 1)
                else:
                    valid = 0
                    for v in state.domain[curnode]:
                        currentchoice[idx] = v
                        if check_valid_combination(currentchoice, idx + 1):
                            valid = 1
                            break
                    return valid

        debugprint(constraintarc)
        nodesinvolved = constraintarc.constraint.nodesinvolved
        nvar = len(nodesinvolved)
        toremove = []
        dsrc = state.domain[constraintarc.srcnode]
        for i, srcvalue in enumerate(dsrc):

            validcombinationexists = check_valid_combination()
            if not validcombinationexists:
                toremove.append(i)

        for i in toremove[-1::-1]: del dsrc[i]

        return len(toremove)

    def enforce_arc_consistency(self, state):
        debugprint(1, ">>Enforcing arc consistency on state ", state)
        constraintset = self.constraintgraph
        ntotalchanges = 0

        # queue = []
        queue = set()
        # initialize: all constraints must be examined at least once
        # for constraintarc in constraintset.arclist:
        for _, constraintarc in constraintset.arclist.iteritems():
            debugprint(1, constraintarc)
            # queue.append(constraintarc)
            queue.add(constraintarc)

        while True:
            try:
                constraintarc = queue.pop()
            except (IndexError, KeyError):
                break  # queue empty
            debugprint(1, "meh:", constraintarc)
            nremovedvalues = self.remove_values(state, constraintarc)
            debugprint(1, "REMOVED", nremovedvalues)

            ntotalchanges += nremovedvalues

            if nremovedvalues > 0:
                # # neighbourset should contain all neighbours, or in alternative
                # # all constraints with an end on the current element
                # for neighbour in constraintset.neighbourlist[constraintarc.srcnode]:
                for neighbour in constraintset.arcsendingin[constraintarc.srcnode]:
                    # queue.append(neighbour)
                    debugprint(1, "Adding neighbour ", neighbour)
                    queue.add(neighbour)

        debugprint(1, "<<Enforcing arc consistency DONE")
        return ntotalchanges

    def solve(self, basestate):
        # make sure the initial state is compliant
        solution = basestate.copy()
        self.enforce_arc_consistency(solution)
        if solution.allvaluesset():
            # we have a solution
            return solution
        elif not solution.isvalid():
            # domain's already dried up
            return None

        # # depth first search
        self.DEBUG_n_solve_iterative = 0
        self.DEBUG_depth_solve_iterative = 0
        found_solution, solution = self.solve_iterative(solution)
        debugprint(0, "TOTAL:", self.DEBUG_n_solve_iterative)
        if found_solution:
            return solution
        else:
            return None

    def solve_iterative(self, state):
        self.DEBUG_n_solve_iterative += 1
        self.DEBUG_depth_solve_iterative += 1
        debugprint(0, "Depth:", self.DEBUG_depth_solve_iterative, "IN")

        statesstack = []

        # if state is has one element per element we have a solution
        if state.allvaluesset():
            # (all the checks have been passed, i.e.)
            # return True
            self.DEBUG_depth_solve_iterative -= 1
            return True, state
        # else
        else:
            # select the node with the least variables
            node = state.get_mindomain()
            # the node ought not to be None, as this case should have been caught previously,
            # and we take for granted that the initial state is both valid and not
            assert (node is not None)
            # toexamine=[]
            toexamine = []
            # for value in domain[node]
            for value in state.domain[node]:
                # push current state
                statesstack.append(state.copy())
                # set domain to value
                state.domain[node] = [value]
                nchanges = self.enforce_arc_consistency(state)

                # if still valid domain
                if state.isvalid():
                    toexamine.append((nchanges, state))

                # 	pop to current state
                # 	(a copy is popped, thus no need to make a further copy)
                state = statesstack.pop()
            #
            # # (now start from the node which implied the fewer changes)
            # for nchanges, values, curstate in sorted(toexamine)
            for nchanges, curstate in sorted(toexamine):
                # state = curstate
                # (again, there should be no need to copy)
                state = curstate
                # is_solution = solve_iterative(state)
                is_solution, solution = self.solve_iterative(state)
                # 	if is_solution
                if is_solution:
                    self.DEBUG_depth_solve_iterative -= 1
                    return True, solution