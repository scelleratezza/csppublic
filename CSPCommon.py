__author__ = 'riccardo campari'

import sys

from collections import defaultdict


def debugprint(level, *args):
    if level <= alertlevel:
        sys.stderr.write(' '.join([str(x) for x in args]) + '\n')


def alerterror(*args):
    message = "ERROR: " + ' '.join([str(x) for x in args]) + '\n'
    sys.stderr.write(message)


alertlevel = 0


class CSPState():
    def __init__(self):
        self.domain = []

    def isvalid(self):
        return all([len(x) > 0 for x in self.domain])

    def allvaluesset(self):
        return all([len(x) == 1 for x in self.domain])

    def __eq__(self, other):
        if isinstance(other, CSPState):
            return self.domain == other.domain
        else:
            return NotImplemented

    def get_mindomain(self):
        lengths = [(len(x), i) for i, x in enumerate(self.domain)]
        try:
            firstvalid = (idx for L, idx in sorted(lengths) if L > 1).next()
        except StopIteration:
            return None
        return firstvalid

    def copy(self):
        ret = CSPState()
        for var in self.domain:
            ret.domain.append([v for v in var])
        return ret

    def set_to_state(self, src):
        self.domain = [v for v in src.domain]

    def __str__(self):
        retstr = "{ "
        for i, d in enumerate(self.domain):
            retstr += "%i: %s, " % (i, str(d))
        retstr = retstr[:-2] + " }"
        return retstr


class Constraint():
    def __init__(self):
        self.nodesinvolved = []
        # # checkfunction should be a function of all the variables in nodesinvolved
        self.checkfunction = None


class ConstraintArc():
    def __init__(self):
        self.srcnode = None
        # self.dstnodes = []
        self.constraint = None


class Constraintset():
    def __init__(self, constraintslist):
        self.arclist = {}
        self.neighbourlist = defaultdict(set)
        self.arcsendingin = defaultdict(set)
        self.arcsstartingin = defaultdict(set)

        self.populate_arclist(constraintslist)

    def populate_arclist(self, constraintslist):
        for constraint in constraintslist:
            for node in constraint.nodesinvolved:
                debugprint(1, node)
                arc = ConstraintArc()
                arc.srcnode = node
                # arc.dstnode = []
                arc.constraint = constraint
                self.arclist[(arc.srcnode, constraint)] = arc
                self.arcsstartingin[arc.srcnode].add(arc)
                for dstnode in arc.constraint.nodesinvolved:
                    self.neighbourlist[node].add(dstnode)
                    self.arcsendingin[dstnode].add(arc)
            self.neighbourlist[node].remove(node)