__author__ = 'riccardo campari'

from CSPSolverBaseClass import *
from random import randint


class CSPMinConflicts(CSPSolverBaseStepperClass):
    def __init__(self, constraintslist):
        CSPSolverBaseStepperClass.__init__(self, constraintslist)
        self.solution = None
        self.currentstate = None
        self.timesteps = 0

    # solve_init : sets up a (random?) initial state by choosing variables in the respective domains
    # solve_next : select a (random?) node, calls get_least_violating_value and changes
    # get_least_violating_value(node) : finds the value of node which violates the fewest constraints

    def count_totalbrokenconstraints(self, state):
        assert (all(len(x) == 1 for x in state.domain))
        nbrokenconstraints = 0
        ntotalconstraints = len(self.constraintslist)
        for constraint in self.constraintslist:
            nodes = constraint.nodesinvolved
            pars = [state.domain[n][0] for n in nodes]
            isvalid = constraint.checkfunction(*pars)

            nbrokenconstraints += 1 - isvalid
        return nbrokenconstraints, ntotalconstraints

    def count_pervalue_brokenconstraints(self, currentstate, node, nodedomain):
        # # compute number of broken constraints per value of the chosen node
        nbrokenconstraints = [0] * len(nodedomain)
        for idx, v in enumerate(nodedomain):
            currentstate.domain[node] = [v]
            n = self.count_brokenconstraints(currentstate, node)
            if 0 == n and self.is_solution(currentstate):
                # is solution !
                self.solution = currentstate
                return None
            else:
                nbrokenconstraints[idx] = n
        return nbrokenconstraints

    def count_brokenconstraints(self, state, node):
        nbrokenconstraints = 0
        dom = state.domain
        assert (all([len(d) == 1 for d in state.domain]))

        for arcconstraint in self.constraintgraph.arcsstartingin[node]:
            constraint = arcconstraint.constraint
            nodes = constraint.nodesinvolved
            pars = [dom[n][0] for n in nodes]
            isvalid = constraint.checkfunction(*pars)
            nbrokenconstraints += 1 - isvalid

        return nbrokenconstraints

    def list_unsatisfied_nodes(self, state):
        ret = []
        for idx in range(len(state.domain)):
            n = self.count_brokenconstraints(state, idx)
            if n > 0:
                ret.append(idx)
        return ret

    def select_random_state(self, srcdomain, dstdomain):
        for idx, srcdom in enumerate(srcdomain):
            dstdomain[idx] = [srcdom[randint(0, len(srcdom) - 1)]]

    def solve_init(self, basestate, startfrom=None):
        self.solution = None
        self.timesteps = 0

        if startfrom is not None:
            currentstate = startfrom.copy()
        else:
            currentstate = basestate.copy()
            # # randomly select starting state
            self.select_random_state(basestate.domain, currentstate.domain)

        self.currentstate = currentstate

        # # is it a solution ?
        if self.is_solution(currentstate):
            self.solution = currentstate
            return True, currentstate
        else:
            # # get list of nodes with unsatisfied constraints
            unsatisfiednodes = self.list_unsatisfied_nodes(currentstate)

            # # load stepper
            self.stepper = Stepper(self.solve_randstep, (basestate, currentstate, unsatisfiednodes))
            return True, None

    def solve_randstep(self, basestate, currentstate, unsatisfiednodes):
        def computenbrokenconstraints(d, n):
            # # compute number of broken constraints per value of the chosen node
            nbc = [0] * len(d[n])
            for idx, v in enumerate(d[n]):
                currentstate.domain[n] = [v]
                n = self.count_brokenconstraints(currentstate, n)
                if 0 == n and self.is_solution(currentstate):
                    # is solution !
                    self.solution = currentstate
                    return None
                else:
                    nbc[idx] = n
            return nbc

        def selectvalue(nbc):
            minn = min(nbc)
            viablelist = [idx for idx, x in enumerate(nbc) if minn == x]
            minidx = viablelist[randint(0, len(viablelist) - 1)]
            # print "VL:", viablelist
            # print minindex
            if 0 == minn:
                unsatisfiednodes.pop(nodeind)
                debugprint(1, "Deleting node %i from unsatisfied nodes; %i left" % (nodeind, len(unsatisfiednodes)))
            return minidx

        # # some debug checks

        assert (all(len(x) > 0 for x in currentstate.domain))
        assert (len(unsatisfiednodes) > 0)
        totalbrokenconstraints, ntotalconstraints = self.count_totalbrokenconstraints(currentstate)

        assert (totalbrokenconstraints > 0)

        # #
        dom = basestate.domain
        self.timesteps += 1
        if 0 == self.timesteps % 1000:
            print totalbrokenconstraints, ntotalconstraints, self.timesteps

        # # chose a random node among those with unsatisfied nodes
        nodeind = randint(0, len(unsatisfiednodes) - 1)
        node = unsatisfiednodes[nodeind]
        debugprint(1, "Selected node:", node)

        # # compute the number of broken constraints for each value in the domain
        nbrokenconstraints = computenbrokenconstraints(dom, node)
        ## check if solution was found
        if nbrokenconstraints is None:
            return Stepper.halt, None, None

        ## select RANDOMLY value with the least broken constraints
        minindex = selectvalue(nbrokenconstraints)

        ## set state to the selected value
        debugprint(1, nbrokenconstraints[minindex], minindex)
        currentstate.domain[node] = [dom[node][minindex]]

        self.currentstate = currentstate

        ## check if solution found
        if 0 == len(unsatisfiednodes):
            ## found solution
            self.solution = currentstate
            return Stepper.halt, None, None
        else:
            ## prepare next step
            head = (self.solve_randstep, (basestate, currentstate, unsatisfiednodes))
            tail = None, None
            return (), head, tail