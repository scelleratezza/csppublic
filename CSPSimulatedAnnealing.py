from CSPMinconflicts import *
from random import random
from math import exp

__author__ = 'riccardo'


class CSPSimulatedAnnealing(CSPMinConflicts):
    def __init__(self, constraintslist, timeschedule, get_nextstate=None):
        CSPMinConflicts.__init__(self, constraintslist)

        assert (hasattr(timeschedule, '__call__'))
        self.timeschedule = timeschedule
        self.get_nextstate = get_nextstate

        self.solve_randstep = self.solve_randstep_external if get_nextstate is not None else self.solve_randstep_vanilla
        self.solution = None
        self.currentstate = None
        self.timestep = 0

    def solve_init(self, basestate, startfrom=None):
        self.solution = None
        self.timestep = 0

        if startfrom is not None:
            currentstate = startfrom.copy()
        else:
            currentstate = basestate.copy()
            # # randomly select starting state
            self.select_random_state(basestate.domain, currentstate.domain)

            # # select variables with non trivial domain
        variabledomains = [idx for idx, dom in enumerate(basestate.domain) if len(dom) > 1]

        # # is it a solution ?
        if self.is_solution(currentstate):
            self.solution = currentstate
            return True, currentstate
        else:
            # # load stepper
            self.stepper = Stepper(self.solve_randstep, (basestate, variabledomains, currentstate))
            return True, None

    def solve_randstep_external(self, basestate, variabledomains, currentstate):
        # # some debug checks
        assert (all(len(x) > 0 for x in currentstate.domain))
        totalbrokenconstraints, totalconstraints = self.count_totalbrokenconstraints(currentstate)
        assert (totalbrokenconstraints > 0)

        # # current temperature:
        self.timestep += 1
        t = self.timeschedule(self.timestep)
        if self.timestep % 1000 == 0:
            print totalbrokenconstraints, totalconstraints, t, self.timestep

        # # get successor
        newstate, deltabrokenstates = self.get_nextstate(currentstate, basestate)

        # # simulated annealing step
        if deltabrokenstates < 0 or random() <= exp(-float(deltabrokenstates) / t):
            currentstate = newstate

        self.currentstate = currentstate

        # check if solution
        if deltabrokenstates < 0 and self.is_solution(currentstate):
            # # found solution
            self.solution = currentstate
            return Stepper.halt, None, None
        else:
            # # setup next step
            # # prepare next step
            head = (self.solve_randstep, (basestate, variabledomains, currentstate))
            tail = None, None
            return (), head, tail

    def solve_randstep_vanilla(self, basestate, variabledomains, currentstate):
        def selectvalue_butvalueidx(nbc, valueidx):
            minn = min([x for idx, x in enumerate(nbc) if idx != valueidx])
            viablelist = [idx for idx, x in enumerate(nbc) if minn == x and idx != valueidx]
            minidx = viablelist[randint(0, len(viablelist) - 1)]
            return minidx, minn

            # # some debug checks

        assert (all(len(x) > 0 for x in currentstate.domain))
        totalbrokenconstraints, totalconstraints = self.count_totalbrokenconstraints(currentstate)
        assert (totalbrokenconstraints > 0)

        # # current temperature:
        self.timestep += 1
        t = self.timeschedule(self.timestep)
        if self.timestep % 1000 == 0:
            print totalbrokenconstraints, totalconstraints, t, self.timestep

        # # domains with more than one possible value

        # # chose a random node
        # node = randint(0, len(currentstate.domain)-1)
        node = variabledomains[randint(0, len(variabledomains) - 1)]
        debugprint(1, "Selected node:", node)
        prev_value = currentstate.domain[node][0]
        prev_idx = (idx for idx, x in enumerate(basestate.domain[node]) if x == prev_value).next()
        prev_constraintsbroken = self.count_brokenconstraints(currentstate, node)

        # # compute the number of broken constraints for each value in the domain
        nbrokenconstraints = self.count_pervalue_brokenconstraints(currentstate, node, basestate.domain[node])
        # # check if solution was found
        if nbrokenconstraints is None:
            return Stepper.halt, None, None

        ## select RANDOMLY value with the least broken constraints
        minindex, new_constraintsbroken = selectvalue_butvalueidx(nbrokenconstraints, prev_idx)
        assert (minindex != prev_idx)
        new_value = basestate.domain[node][minindex]
        currentstate.domain[node] = [new_value]

        ## simulated annealing step
        delta = new_constraintsbroken - prev_constraintsbroken
        if delta > 0 and random() > exp(-float(delta) / t):
            currentstate.domain[node] = [prev_value]

        self.currentstate = currentstate

        # check if solution
        if 0 == new_constraintsbroken and self.is_solution(currentstate):
            ## found solution
            self.solution = currentstate
            return Stepper.halt, None, None
        else:
            ## setup next step
            ## prepare next step
            head = (self.solve_randstep, (basestate, variabledomains, currentstate))
            tail = None, None
            return (), head, tail