import numpy as np
import sudoku

N = 9
s = sudoku.Sudoku(N)


def check_constraint():
    c1 = np.arange(1, N + 1)
    assert (True == s.constraint(*c1))

    c2 = np.copy(c1)
    c2[0] = N
    assert (False == s.constraint(*c2))

    c3 = np.arange(N)
    assert (False == s.constraint(*c3))


def construct_solution():
    board = np.zeros((N, N), dtype=np.int)
    is_square, root = sudoku.Sudoku.is_perfect_square(N)
    root = root if root else 1
    for row in range(N):
        board[row, :] = 1 + ((np.arange(N) + row * root + (row // root)) % N)

    boarddomain = [[int(x)] for x in np.nditer(board)]
    boardstate = sudoku.CSPState()
    boardstate.domain = boarddomain
    return boardstate


def check_good_solution():
    board = construct_solution()
    # solution = s.csp.solve(board)
    # solution = s.cspmin.solve(board)
    solution = s.cspsa.solve(board)
    # assert( board.domain == solution.domain )
    print board
    print solution
    assert (board == solution)


def create_enigma():
    fixedvalues = [
        (0, 2, 5), (0, 5, 2), (0, 7, 3),
        (1, 0, 8), (1, 3, 6),
        (2, 2, 9), (2, 4, 1), (2, 6, 7), (2, 8, 8),
        (3, 0, 6), (3, 5, 8), (3, 7, 9),
        (4, 2, 1), (4, 6, 4),
        (5, 1, 3), (5, 3, 7), (5, 8, 5),
        (6, 0, 2), (6, 2, 7), (6, 4, 5), (6, 6, 8),
        (7, 5, 3), (7, 8, 6),
        (8, 1, 4), (8, 3, 9), (8, 6, 1)
    ]
    return s.makestate_fromboundvalues(fixedvalues)


def create_dailysudoku():
    fixedvalues = \
        [(0, j, v) for j, v in [(5, 3), (6, 8), (8, 5)]] + \
        [(1, j, v) for j, v in [(2, 7), (3, 4), (8, 6)]] + \
        [(2, j, v) for j, v in [(1, 1), (5, 8)]] + \
        [(3, j, v) for j, v in [(0, 1), (1, 6), (4, 8), (5, 5)]] + \
        [(4, j, v) for j, v in [(1, 9), (7, 6)]] + \
        [(5, j, v) for j, v in [(3, 6), (4, 9), (7, 8), (8, 2)]] + \
        [(6, j, v) for j, v in [(3, 8), (7, 2)]] + \
        [(7, j, v) for j, v in [(0, 3), (5, 6), (6, 1)]] + \
        [(8, j, v) for j, v in [(0, 9), (2, 4), (4, 1)]]
    return s.makestate_fromboundvalues(fixedvalues)


def create_dailysudoku_24072014_med():
    fixedvalues = \
        [(0, j, v) for j, v in [(2, 9), (3, 6), (6, 3)]] + \
        [(1, j, v) for j, v in [(4, 3), (5, 1)]] + \
        [(2, j, v) for j, v in [(0, 5), (3, 8), (4, 4), (8, 7)]] + \
        [(3, j, v) for j, v in [(0, 9), (2, 1), (7, 7), (8, 3)]] + \
        [(4, j, v) for j, v in [(2, 2), (6, 6)]] + \
        [(5, j, v) for j, v in [(0, 8), (1, 7), (6, 4), (8, 1)]] + \
        [(6, j, v) for j, v in [(0, 6), (4, 1), (5, 3), (8, 2)]] + \
        [(7, j, v) for j, v in [(3, 5), (4, 6)]] + \
        [(8, j, v) for j, v in [(2, 5), (5, 7), (6, 9)]]
    return s.makestate_fromboundvalues(fixedvalues)


check_constraint()

check_good_solution()

# #

sett_enigm = create_enigma()
s.print_state(sett_enigm)
# s.csp.solve(sett_enigm)
# s.cspmin.solve(sett_enigm)
# s.cspsa.solve(sett_enigm)

# s.print_state( s.cspsa.stepper._next[1][-1] )
# newstate = s.cspsa.stepper._next[1][-1].copy()
# newstate = s.cspmin.stepper._next[1][1].copy()
# s.csp.solve(newstate)
# s.cspmin.solve(newstate)

# #
second_enigm = create_dailysudoku()

s.print_state(second_enigm)
sol = s.csp.solve(second_enigm)
s.print_state(second_enigm)
s.print_state(sol)
# s.cspmin.solve(second_enigm)
# s.cspsa.solve(second_enigm)

##
third_enigm = create_dailysudoku_24072014_med()
s.print_state(third_enigm)
sol = s.csp.solve(third_enigm)

s.print_state(third_enigm)
s.print_state(sol)
# sol = s.cspi.solve(third_enigm)
# s.cspsa.solve(third_enigm)

# newstate = s.cspsa.currentstate
# s.cspmin.solve(third_enigm, newstate)
# s.solve_sao(third_enigm, newstate)
