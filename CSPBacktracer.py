from CSPCommon import *
from CSPSolverBaseClass import CSPSolverBaseClass


class CSPBacktracer(CSPSolverBaseClass):
    def __init__(self, constraintslist):
        CSPSolverBaseClass.__init__(self, constraintslist)

    def solve(self, basestate):
        # make sure the initial state is compliant
        solution = basestate.copy()
        self.enforce_arc_consistency(solution)
        if solution.allvaluesset():
            # we have a solution
            return solution
        elif not solution.isvalid():
            # domain's already dried up
            return None

            # # depth first search
        self.DEBUG_n_solve_iterative = 0
        self.DEBUG_depth_solve_iterative = 0
        found_solution, solution = self.solve_iterative(solution)
        debugprint(1, "TOTAL:", self.DEBUG_n_solve_iterative)
        if found_solution:
            return solution
        else:
            return None

    def solve_iterative(self, state):
        self.DEBUG_n_solve_iterative += 1
        self.DEBUG_depth_solve_iterative += 1
        debugprint(1, "Depth:", self.DEBUG_depth_solve_iterative, "IN")

        statesstack = []

        # if state is has one element per element we have a solution
        if state.allvaluesset():
            # (all the checks have been passed, i.e.)
            # return True
            self.DEBUG_depth_solve_iterative -= 1
            return True, state
        # else
        else:
            # select the node with the least variables
            node = state.get_mindomain()
            # the node ought not to be None, as this case should have been caught previously,
            # and we take for granted that the initial state is both valid and not
            assert (node is not None)
            toexamine = []
            # for value in domain[node]
            for value in state.domain[node]:
                # push current state
                statesstack.append(state.copy())
                # set domain to value
                state.domain[node] = [value]
                # nchanges = enforce arc consistency
                nchanges = self.enforce_arc_consistency(state)
                # if still valid domain
                if state.isvalid():
                    # 		toexamine.append( nchanges, value, state )
                    toexamine.append((nchanges, state))
                # 	pop to current state
                # 	(copy is popped, thus no need to make a further copy)
                state = statesstack.pop()
            #
            # # (now start from the node which implied the fewer changes)
            # for nchanges, values, curstate in sorted(toexamine)
            for nchanges, curstate in sorted(toexamine):
                # state = curstate
                # (again, there should be no need to copy)
                state = curstate
                # is_solution = solve_iterative(state)
                is_solution, solution = self.solve_iterative(state)
                # 	if is_solution
                if is_solution:
                    self.DEBUG_depth_solve_iterative -= 1
                    return True, solution
                    # 		return True
