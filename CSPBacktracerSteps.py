#
# TODO:
# 1) IMPLEMENT depth 1 arc consistency
# 2) MAKE the algorithm flexible and controllable
# 3) IMPLEMENT argument function which yields the new values for a given node
# NB: You still need to perform a FULL constraint check, because the solution is reached based
# on the assumption that the current state is a valid one .. An alternative could be to give a
# preferential order, but it would unlikely change much. Another alternative is to allow users
#       to implement their own arc consistency functions.
# 4) TREE algorithm or reduction to tree
# 5) LOCAL solutions starting from complete assignment and improving on the number of violated constraints
#

from CSPSolverBaseClass import *
from heapq import *

alertlevel = 0


class CSPBacktracerSteps(CSPSolverBaseStepperClass):
    def __init__(self, constraintslist, examine_more_limiting_first=False):
        CSPSolverBaseStepperClass.__init__(self, constraintslist)
        # this decides whether we first explore the least or the more limiting of the
        # values we can attribute to a given node
        self.nchangesmultiplier = -1 if examine_more_limiting_first else 1
        self.solution = None

    def solve_init(self, basestate, startfrom=None):
        debugprint(0, "INSIDE solve")

        self.solution = None

        # make sure the initial state is compliant
        solution = basestate.copy()
        # self.enforce_arc_consistency(solution)
        if solution.allvaluesset():
            # we have a solution
            return True, solution
        elif not solution.isvalid():
            # domain's already dried up
            return False, None

        ## depth first search
        self.DEBUG_n_solve_iterative = 0
        self.DEBUG_depth_solve_iterative = 0

        self.stepper = Stepper(self.solve_iterative_get_nchanges_head, solution)
        return True, None

    def solve_iterative_get_nchanges_head(self, state):
        debugprint(0, "INSIDE solve_iterative_get_nchanges_HEAD")
        # if state is has one element per element we have a solution
        if state.allvaluesset():
            # all the checks have been passed
            self.solution = state
            return Stepper.halt, None, None
        # else
        else:
            # get node with the smallest number of available options
            node = state.get_mindomain()
            # the node ought not to be None, as this case should have been caught previously,
            # and we take for granted that the initial state is both valid and not empty
            assert (node is not None)

            values = state.domain[node]

            toexamine = []

            head = (self.solve_iterative_get_nchanges_body, (state, node, values, toexamine))
            tail = (self.solve_iterative_loop_on_values, (toexamine,))

            return values, head, tail

    def solve_iterative_get_nchanges_body(self, state, node, values, toexamine):
        debugprint(0, "INSIDE solve_iterative_get_nchanges_BODY")
        if 0 == len(values):
            return None, None, None
        value = values.pop()
        # modify tomodifystate, and pass this to successive functions
        tomodifystate = state.copy()

        tomodifystate.domain[node] = [value]
        nchanges = self.enforce_arc_consistency(tomodifystate, node)
        # check if we got a solution
        if tomodifystate.allvaluesset():
            self.solution = tomodifystate
            return Stepper.halt, None, None
        # 	if still valid domain
        if tomodifystate.isvalid():
            # toexamine.append( nchanges, value, tomodifystate )
            # toexamine.append((nchanges, tomodifystate))
            heappush(self.nchangesmultiplier * toexamine, (nchanges, tomodifystate))

        head = (self.solve_iterative_get_nchanges_body, (state, node, values, toexamine))
        tail = None

        return (nchanges, tomodifystate), head, tail

    def solve_iterative_loop_on_values(self, toexamine):
        if 0 == len(toexamine):
            return None, None, None

        _, state = heappop(toexamine)

        head = (self.solve_iterative_get_nchanges_head, (state,))
        tail = (self.solve_iterative_loop_on_values, (toexamine,))

        return state, head, tail