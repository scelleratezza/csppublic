__author__ = 'riccardo'
from CSPCommon import *
from Stepper import *


class CSPSolverBaseClass():
    def __init__(self, constraintslist):
        self.constraintslist = constraintslist
        self.constraintgraph = Constraintset(constraintslist)
        self.solution = None

    def remove_values(self, state, constraintarc):
        def check_valid_combination(currentchoice=None, idx=0):
            if currentchoice is None:
                currentchoice = [None] * nvar

            if nvar == idx:
                return constraintarc.constraint.checkfunction(*currentchoice)
            else:
                curnode = nodesinvolved[idx]
                if curnode == constraintarc.srcnode:
                    currentchoice[idx] = srcvalue
                    return check_valid_combination(currentchoice, idx + 1)
                else:
                    valid = 0
                    for v in state.domain[curnode]:
                        currentchoice[idx] = v
                        if check_valid_combination(currentchoice, idx + 1):
                            valid = 1
                            break
                    return valid

        debugprint(1, constraintarc)
        nodesinvolved = constraintarc.constraint.nodesinvolved
        nvar = len(nodesinvolved)
        toremove = []
        dsrc = state.domain[constraintarc.srcnode]
        for i, srcvalue in enumerate(dsrc):
            validcombinationexists = check_valid_combination()
            if not validcombinationexists:
                toremove.append(i)

        for i in toremove[-1::-1]: del dsrc[i]

        return len(toremove)

    # def enforce_arc_consistency(self, state):
    def enforce_arc_consistency(self, state, node=None, only_one_layer=False):
        #
        # enforces arcs after a change in the domain of node
        # if node is None, all nodes are updated
        #
        totalremovedvalues = 0
        if node is None:
            for n in range(len(state.domain)):
                totalremovedvalues += self.enforce_arc_consistency(state, n)
            return totalremovedvalues

        debugprint(1, ">>Enforcing arc consistency on state ", state)
        constraintset = self.constraintgraph

        # queue = []
        queue = set()
        # initialize: all constraints must be examined at least once
        # for constraintarc in constraintset.arclist:
        # for _, constraintarc in constraintset.arclist.iteritems():
        for constraintarc in constraintset.arcsendingin[node]:
            debugprint(1, constraintarc)
            # queue.append(constraintarc)
            queue.add(constraintarc)

        while True:
            try:
                constraintarc = queue.pop()
            except (IndexError, KeyError):
                break  # queue empty
            debugprint(0, "currently %i arcs in the queue" % len(queue))
            debugprint(1, "meh:", constraintarc)
            nremovedvalues = self.remove_values(state, constraintarc)
            debugprint(1, "REMOVED", nremovedvalues)

            if nremovedvalues > 0:
                # # neighbourset should contain all neighbours, or in alternative
                # # all constraints with an end on the current element
                # for neighbour in constraintset.neighbourlist[constraintarc.srcnode]:
                totalremovedvalues += nremovedvalues
                if not only_one_layer:
                    for neighbour in constraintset.arcsendingin[constraintarc.srcnode]:
                        # queue.append(neighbour)
                        debugprint(1, "Adding neighbour ", neighbour)
                        queue.add(neighbour)

        debugprint(1, "<<Enforcing arc consistency DONE")
        return totalremovedvalues

    def solve(self, basestate):
        alerterror("SOLVE of the base class CSBSolverBaseClass called")


class CSPSolverBaseStepperClass(CSPSolverBaseClass):
    class CSPSolverNotInitialized(Exception):
        def __init__(self):
            pass

        def __str__(self):
            return "CSPSolver not initialized before calling next"

    def __init__(self, constraintslist):
        CSPSolverBaseClass.__init__(self, constraintslist)
        self.stepper = None

    def is_solution(self, state):
        dom = state.domain
        if any([len(d) != 1 for d in state.domain]):
            return False

        for constraint in self.constraintslist:
            nodes = constraint.nodesinvolved
            pars = [dom[n][0] for n in nodes]
            if not constraint.checkfunction(*pars):
                return False
        return True

    def solve_init(self, basestate, startfrom=None):
        alerterror("SOLVE of the base class CSBSolverBaseClass called")

    def next(self):
        if self.stepper:
            return self.stepper.next()
        else:
            raise self.CSPSolverNotInitialized()

    def solve(self, basestate, startfrom=None):
        debugprint(1, "INSIDE solve")
        self.solution = None

        is_valid, solution = self.solve_init(basestate, startfrom)
        if is_valid:
            if solution is not None:
                return solution
            else:
                while self.stepper.next():
                    pass
            solution = self.solution
            return solution
        else:
            return None