from Stepper import Stepper


def factorial_body(state, n):
    print "factorial_body"
    if 0 == n:
        next_step = None  # (None, [])
    else:
        state[0] *= n
        next_step = (factorial_body, (state, n - 1))
    tail = None  # (None, [])
    return None, next_step, tail


def factorial_tail(state):
    print "factorial_tail"
    return state[0], None, None


def factorial(n):
    print "factorial"
    state = [1]
    next_step = (factorial_body, (state, n))
    tail = (factorial_tail, (state,))
    return None, next_step, tail


step = Stepper(factorial, 5)
c = 1
while True:
    print "Step n. %i" % c
    cont = step.next()
    print "\tRet value: %s" % str(step._retval)
    print "\tNext params: %s" % str(step.next)

    c += 1
    if not cont:
        break